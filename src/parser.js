const fetch = require('node-fetch');
const cheerio = require('cheerio');
const range = require('lodash/range');

const { fetchHTML } = require('./helpers');

module.exports = {
  buildPage
};

async function buildPage(url) {
  const html = await fetchHTML(url);
  let $ = cheerio.load(html);
  const pages = getPages(html);
  const paginator = createPaginator(pages);

  $ = removeClutter($);
  $('head').append(($('<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>')));

  const firstList = $('div.posts-list').clone();
  firstList.attr('id', 1);

  const content = $('<div class="content"></div>');
  // content.css('margin-top', '100px');

  $('body')
    .html('')
    // .append(paginator)
    .append(content);

  content.append(getTitle(1));
  content.append(processList(firstList, 1));

  await pages.reduce(async (acc, page) => {
    await acc;
    let pageHtml = await fetchHTML(page.url);
    let $$ = cheerio.load(pageHtml);
    let list = processList($$('div.posts-list'), page.num);
    list.attr('id', page.num);
    // content.find('.posts-list').append(list.find('article'));
    content.append(getTitle(page.num));
    content.append(list);
  }, Promise.resolve(1));

  return $.html();
}

function getTitle(i) {
  return `<h3 style="margin: 10px 20px;">Page ${i}</h3><hr/>`;
}

function createPaginator(pages) {
  let navStyles = "position: fixed; height: 60px; display: flex; align-items: center; margin: 0 20px; top: 0; left: 0; right: 0; background: white; z-index: 1;";
  return [
    `<nav style="${navStyles}">`,
      '<h3><a href="#1">Page 1&emsp;</a></h3>',
      ...pages.map((page)=>`<h3><a href="#${page.num}">Page ${page.num}&emsp;</a></h3>`),
    '</nav>'
  ].join('');
}

function getPages(html) {
  const $ = cheerio.load(html);
  let start = $('.main-pagination a:first-of-type').text();
  let end = $('.main-pagination .next').prev().text();
  return range(parseInt(start), parseInt(end) + 1).map((e) => {
      return {
        num: e,
        url: `https://howtomakeelectronicmusic.com/category/tutorials/page/${e}/`
      };
    });
}

function removeClutter($) {
  $('head script').each((i, e) => {
    if (!Object.keys(e.attribs).length) {
     $(e).remove();
    }
  });
  $('.top-bar').remove();
  $('.main-head').remove();
  $('.breadcrumbs-wrap').remove();
  $('.main-footer').remove();
  $('.main-wrap aside').remove();
  return $;
}

function processList(list, anchor) {
  list.attr('id', anchor);
  list.find('a').attr('target', '_blank');
  list
  .css({
    display: 'flex',
    'flex-direction': 'row',
    'flex-wrap': 'wrap',
    margin: '20px'
  });

  list.find('.cat-title.cat-3').remove();
  list.find('.read-more').remove();

  list.find('article a img')
    .removeClass('no-display')
    .css({
      'width': '200px',
      'height': '100px',
    });

  list.find('article').css({
    'flex-grow': '1',
    'flex-shrink': '1',
    'flex-basis': 'calc(25% - 80px)',
    padding: '10px',
    display: 'flex',
    'flex-direction': 'column',
    'align-items': 'flex-start',
  });

  list.find('article .content').css({
    'margin-left': '0px',
  });

  list.find('.content a').css('margin','0');
  list.find('.content .excerpt').css('margin','0');
  return list;
}
