const fetch = require('node-fetch');

module.exports = {
  fetchHTML
};

async function fetchHTML(url) {
  // console.log('fetching url', url);
  const res = await fetch(url);
  const html = await res.text();
  return html;
}
