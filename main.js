const { writeFile } = require('fs');
const { promisify } = require('util');
const htmlPrettify = require('js-beautify').html;
// const { exec } = require('child_process');

const write = promisify(writeFile);
const { buildPage } = require('./src/parser');

const baseUrl = "https://howtomakeelectronicmusic.com/category/tutorials/";
(async () => {

  let html = await buildPage(baseUrl);
  html = htmlPrettify(html);
  await write('./output/htmem.html', html, 'utf8');

})()
.catch((err) => {
  console.error(err)
;});
